## 1.5.0

- HttpException refactoring + enhancements

## 1.4.0

- UploaderRequest#body => UploaderRequest#file

## 1.3.0

- Добавить в ApiClient  возможность работы с BrowserClient  со свойством withCredentials
- Upgrade to dart 2.7

## 1.2.0

- Поддержка multipart/form-data

## 1.1.0

- allow block request in onBeforeRequest
- added requestInProgress status

## 1.0.1

- added content-length header to uploader
- added content-type header processing

## 1.0.0

- fixed dependency on data_model v1.0.0
  
## 0.2.0

- added uploader
- http-client is created now automatically

## 0.1.1

- createObject => createModel
- changed library description

## 0.1.0

- RestClient => ApiClient
- RestResource => ResourceClient

## 0.0.1

- Initial version
