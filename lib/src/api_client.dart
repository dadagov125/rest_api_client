import 'dart:async';
import 'dart:convert';

import 'package:data_model/data_model.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'package:universal_io/prefer_universal/io.dart' show ContentType;

import 'api_request.dart';
import 'api_response.dart';
import 'multipart_request.dart';
import 'upload_request.dart';
import 'uploader.dart';

typedef OnAfterResponse = Future<ApiResponse> Function(ApiResponse response);
typedef OnBeforeRequest = Future<ApiRequest> Function(ApiRequest request);

/// API client
///
/// Implements method `send` to communicate with api-server
class ApiClient {
  final http.BaseClient _httpClient;
  final _upload = Uploader();
  Uri _apiUri;
  final OnBeforeRequest _onBeforeRequest;
  final OnAfterResponse _onAfterResponse;

  /// Creates new ApiClient
  ///
  /// [apiUri] - API-server uri
  ///
  /// [onBeforeRequest] callback, which takes as argument [ApiRequest] and
  /// returns [ApiRequest]. This callback is usually used to add something to
  /// every api request. If [onBeforeRequest] returns `null` - the request will
  /// be skipped and [send]-method returns also `null`
  ///
  /// [onAfterResponse] callback, which takes as argument [ApiResponse] and returns [ApiResponse].
  /// This callback is usually used to get some additional info from response data
  ApiClient(Uri apiUri,
      {http.BaseClient httpClient,
      OnBeforeRequest onBeforeRequest,
      OnAfterResponse onAfterResponse})
      : _httpClient = httpClient ?? http.Client(),
        _apiUri = apiUri,
        _onBeforeRequest = onBeforeRequest,
        _onAfterResponse = onAfterResponse {
    if (apiUri == null) throw ArgumentError.notNull('apiUri');
  }

  /// Base API URI
  Uri get apiUri => _apiUri;

  set apiUri(Uri uri) {
    if (uri == null) throw ArgumentError.notNull('uri');
    _apiUri = uri;
  }

  /// Sends the request to the API-server.
  Future<ApiResponse> send(ApiRequest request) async {
    if (_onBeforeRequest != null) request = await _onBeforeRequest(request);
    if (request == null) return null;
    var requestUri = _apiUri.replace(
        path: url.normalize(url.join(_apiUri.path, request.resourcePath)),
        queryParameters: request.queryParameters);

    http.Response response;
    ApiResponse apiResponse;
    _requestInProgress = true;
    try {
      if (request is UploadRequest) {
        response = await _upload(request.body, requestUri,
            headers: request.headers,
            onProgress: request.onProgress,
            onComplete: request.onComplete,
            onError: request.onError);
      } else if (request is MultipartRequest) {
        var multipartRequest =
            http.MultipartRequest(request.methodName, requestUri);
        multipartRequest.fields.addAll(request.fields);
        multipartRequest.headers.addAll(request.headers);
        multipartRequest.files.addAll(request.files);
        response =
            await http.Response.fromStream(await multipartRequest.send());
      } else {
        switch (request.method) {
          case HttpMethod.get:
            response =
                await _httpClient.get(requestUri, headers: request.headers);
            break;
          case HttpMethod.post:
            response = await _httpClient.post(requestUri,
                headers: request.headers,
                body: json.encode(request.body, toEncodable: _toEncodable));
            break;
          case HttpMethod.put:
            response = await _httpClient.put(requestUri,
                headers: request.headers,
                body: json.encode(request.body, toEncodable: _toEncodable));
            break;
          case HttpMethod.patch:
            response = await _httpClient.patch(requestUri,
                headers: request.headers,
                body: json.encode(request.body, toEncodable: _toEncodable));
            break;
          case HttpMethod.delete:
            response =
                await _httpClient.delete(requestUri, headers: request.headers);
            break;
          default:
            throw UnsupportedError('Request method is not supported');
        }
      }
    } catch (e) {
      rethrow;
    } finally {
      _requestInProgress = false;
    }
    apiResponse = ApiResponse(
        statusCode: response.statusCode,
        reasonPhrase: response.reasonPhrase,
        headers: response.headers,
        body: _extractBody(response));

    if (_onAfterResponse != null) apiResponse = await _onAfterResponse(apiResponse);

    return apiResponse;
  }

  dynamic _extractBody(http.Response response) {
    final supportedTextMimeTypes = List<String>.unmodifiable(
        ['text/plain', 'text/html', 'text/css', 'text/xml', 'text/csv']);
    final supportedBynaryMimeTipes = List<String>.unmodifiable([
      'image/gif',
      'image/jpeg',
      'image/png',
      'application/vnd.ms-excel',
      'application/msword',
      'application/pdf',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      'application/octet-stream'
    ]);
    final contentType = response.headers['content-type'] == null
        ? null
        : ContentType.parse(response.headers['content-type']);
    if (contentType?.mimeType == ContentType.json.mimeType ||
        contentType == null) {
      return response.body.isNotEmpty ? json.decode(response.body) : '';
    } else if (supportedTextMimeTypes.contains(contentType.mimeType)) {
      return response.body;
    } else if (supportedBynaryMimeTipes.contains(contentType.mimeType)) {
      return response.bodyBytes;
    } else {
      throw UnsupportedError(
          'Unsupported response mime-type: ${contentType.mimeType}');
    }
  }

  dynamic _toEncodable(value) {
    if (value is DateTime) {
      return value.toUtc().toIso8601String();
    } else if (value is JsonEncodable) {
      return value.json;
    } else {
      throw FormatException('Cannot encode to JSON value: $value');
    }
  }

  bool _requestInProgress = false;

  /// Request progress status
  ///
  /// `true` if request is in progress. Otherwise - `false`
  bool get requestInProgress => _requestInProgress;
}
