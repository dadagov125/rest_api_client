import 'dart:convert';
import 'dart:io';
import 'dart:math';

import "package:stream_channel/stream_channel.dart";

hybridMain(StreamChannel channel) async {
  int port = 3000 + Random().nextInt(1000);
  final server = await HttpServer.bind(InternetAddress.loopbackIPv4, port);
  channel.sink.add('${server.address.address}:${server.port}');
  await for (HttpRequest request in server) {
    request.response.headers
      ..add('Access-Control-Allow-Origin', '*')
      ..add('Access-Control-Allow-Methods',
          'GET, POST, PUT, PATCH, DELETE, OPTIONS')
      ..add('Access-Control-Allow-Headers',
          'Origin, X-Requested-With, Content-Type, Accept, Content-Disposition');
    if (request.headers.value('Accept') == 'application/octet-stream') {
      request.response.headers
          .add('Content-Type', ContentType.binary.toString());
    } else {
      request.response.headers.add('Content-Type', ContentType.json.toString());
    }
    if (request.method != 'OPTIONS') {
      if (request.headers.value('X-Requested-With') != 'XMLHttpRequest') {
        request.response.statusCode = HttpStatus.badRequest;
        request.response.write('Not AJAX-request');
      } else {
        switch (request.requestedUri.pathSegments.first) {
          case 'unauthorized':
            request.response.statusCode = HttpStatus.unauthorized;
            request.response.reasonPhrase =
                '${HttpStatus.unauthorized}-Unauthorized';
            break;
          case 'servererror':
            request.response.statusCode = HttpStatus.internalServerError;
            request.response.reasonPhrase =
                '${HttpStatus.internalServerError}-Internal Server Error';
            break;
          case 'unsupported-mime':
            request.response.statusCode = HttpStatus.ok;
            request.response.headers.contentType =
                ContentType('application', 'javascript');
            break;
          case 'echo-resource':
            switch (request.method) {
              case 'GET':
              case 'DELETE':
                if (request.uri.queryParameters.isEmpty &&
                    request.uri.pathSegments.length > 1) {
                  request.response.write(json.encode({'id': 1}));
                } else {
                  request.response.write(json.encode([
                    {'id': 1},
                    {'id': 2}
                  ]));
                }
                break;
              case 'POST':
              case 'PUT':
              case 'PATCH':
                String body = await request.map(utf8.decode).join();
                request.response.write(body);
                break;
            }
            break;
          case 'form-multipart':
            if (request.method == 'POST') {
              String contentLength = request.headers.value('Content-Length');
              String contentType = request.headers.value('Content-Type');
              request.response.write(jsonEncode({
                'Content-Type': contentType,
                'Content-Length': contentLength,
              }));
            }
            break;
          case 'files':
            if (request.method == 'POST') {
              String fileType = request.headers.contentType.mimeType;
              String fileName = request.headers
                  .value('content-disposition')
                  ?.split('=')
                  ?.last
                  ?.trim();
              final fileBytes = await request
                  .map((l) => List<int>.from(l))
                  .reduce((fileBytes, data) => fileBytes..addAll(data));
              request.response.write(jsonEncode({
                'type': fileType,
                'name': fileName,
                'bytesCount': fileBytes.length
              }));
            }
            break;
          default:
            switch (request.method) {
              case 'GET':
                request.response.write(json.encode(
                    {'method': 'GET', 'uri': '${request.requestedUri}'}));
                break;
              case 'POST':
                String body = await request.map(utf8.decode).join();
                request.response.write(json.encode({
                  'method': 'POST',
                  'uri': '${request.requestedUri}',
                  'body': json.decode(body)
                }));
                break;
              case 'PUT':
                String body = await request.map(utf8.decode).join();
                request.response.write(json.encode({
                  'method': 'PUT',
                  'uri': '${request.requestedUri}',
                  'body': json.decode(body)
                }));
                break;
              case 'PATCH':
                String body = await request.map(utf8.decode).join();
                request.response.write(json.encode({
                  'method': 'PATCH',
                  'uri': '${request.requestedUri}',
                  'body': json.decode(body)
                }));
                break;
              case 'DELETE':
                request.response.write(json.encode(
                    {'method': 'DELETE', 'uri': '${request.requestedUri}'}));
                break;
            }
        }
      }
    }
    request.response.close();
  }
}
